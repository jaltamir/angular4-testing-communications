import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()

export class HijoService {
    public cambioNombre: object = {
        'viejoNombre' : '',
        'nuevoNombre' : ''
    };

    public descBehavior   = new BehaviorSubject<object>(this.cambioNombre);
    public descObservable = this.descBehavior.asObservable();

    comunicarCambioNombre(viejoNombre, nuevoNombre) {
        this.cambioNombre = {
            viejoNombre : viejoNombre,
            nuevoNombre : nuevoNombre
        };
        this.descBehavior.next(this.cambioNombre);
    }
}
