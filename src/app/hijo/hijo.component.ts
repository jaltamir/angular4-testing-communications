import { Subscription } from 'rxjs/Rx';
import { Component, Input, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core';
import { HijoService } from './hijo.service';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.scss'],
  providers: [HijoService]
})
export class HijoComponent implements OnInit, OnDestroy {

  @Output() cambioNombreEvent = new EventEmitter<string>();
  @Input() nombre = '';
  @Input() nuevoNombre = '';
  private ultimoEvento = '';
  private cambioNombreSubscription:Subscription = null;
  constructor(public hijoService: HijoService) { }

  ngOnInit(): void {
    this.cambioNombreSubscription = this.hijoService.descObservable.subscribe(
      data => {
        if (data['nuevoNombre'] !== '' && this.nombre !== data['nuevoNombre']) {
          const evento = 'Nombre cambiado de ' + data['viejoNombre'] + ' a ' + data['nuevoNombre'];
          this.nombre = data['nuevoNombre'];
          this.cambioNombreEvent.emit(evento);
          this.ultimoEvento = evento;
        }
      });
  }

  ngOnDestroy() {
    this.cambioNombreSubscription.unsubscribe;
  }

  cambiarNombre(event) {
    this.hijoService.comunicarCambioNombre(this.nombre, this.nuevoNombre);
  }
}
